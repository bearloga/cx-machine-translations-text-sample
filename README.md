# cx-machine-translations-text-sample

Code for retrieving and putting together parallel text format of Content Translation sample publications for [T304453](https://phabricator.wikimedia.org/T304453)

For documentation on the API see [mw:Content translation/Published translations § API](https://www.mediawiki.org/wiki/Content_translation/Published_translations?useskin=vector-2022#API)

> By default, the section contents are HTML. But if you prefer to get plain text version of each section, use `striphtml` argument in the API.

From [T300977#7700725](https://phabricator.wikimedia.org/T300977#7700725):

> Use https://api-ro.discovery.wmnet and set the HTTP Host header to the domain of the site you want to access, e.g. www.wikidata.org

To use that API locally (not a stat host) start a socket server to use as proxy for querying locally:

```bash
ssh -D 8080 -f -C -q -N stat1004.eqiad.wmnet
```

Then set the proxy in R:

```r
Sys.setenv(HTTPS_PROXY = "socks5h://0:8080")
```

To stop the server: `ps x | grep stat1004` and `kill ####`
